Rails.application.routes.draw do
root 'posts#new'
get 'about' => 'pages#about'
resources :posts
end
