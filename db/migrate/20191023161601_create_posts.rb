class CreatePosts < ActiveRecord::Migration[6.0]
  def change
    create_table :posts do |t|
      t.string :Name
      t.string :Deployment
      t.string :Region
      t.string :Release_server

      t.timestamps
    end
  end
end
